<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>La pisciculture PHP</title>
    <link rel="stylesheet" href="css/styles.css" />
</head>

<body>
    <div id="conteneur">
        <header>
            <div class="login">
                <?php
                if (isset($_SESSION['logged_in']['login']) == TRUE) {
                    //l'internaute est authentifié
                    echo $_SESSION['logged_in']['prenom'] . ' ' . $_SESSION['logged_in']['nom'];
                    //affichage "Se déconnecter"(logout.php), "Prif", "Paramètres", etc ...
                ?>
                    <a href="logout.php">- Deconnexion</a>
                    <?php } else {
                    //Personne n'est authentifié
                    // affichage d'un lien pour se connecter
                    // <a href="login.php">Connexion</a>

                    ?>

                    <a href="login.php">Connexion</a>
                <?php
                }
                ?>            
            </div>

            <h1>La pisciculture PHP</h1>
        </header>

        <nav>
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <li><a href="bassins.php">Les bassins</a></li>
                <li><a href="arcenciel.php">La truite arc-en-ciel</a></li>
                <?php
                if (isset($_SESSION['logged_in']['login']) == TRUE) {
                    //l'internaute est authentifié?>
                    <li><a href="ajouterbassin.php">Ajouter un bassin</a></li>
                    <li><a href="supprbassin.php">Supprimer un bassin</a></li>
                    <?php } else {
                    //Personne n'est authentifié
                    // affichage d'un lien pour se connecter
                    // <a href="login.php">Connexion</a>

                    ?>
                <?php
                }
                ?> 

            </ul>
        </nav>
        <section>

            <?php echo $contenu; ?>



        </section>

        <footer>
            <p>Copyright Truites PHP - Tous droits réservés -
                <a href="#">Contact</a>
            </p>
        </footer>
    </div>
</body>

</html>