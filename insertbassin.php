<?php
require "bdd/bddconfig.php";

$paramOK = false;
// Recup les 3 variables POST et les sécurise
if ((isset($_POST['nom'])) && (isset($_POST['descript'])) && (isset($_POST['refCapteur']))) {
    $nom = htmlspecialchars($_POST['nom']);
    $descript = htmlspecialchars($_POST['descript']);
    $refCapteur = intval(htmlspecialchars($_POST['refCapteur']));
    $paramOK = true;
}

// INSERT dans la base
if ($paramOK == true) {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $addBassin = $objBdd->prepare("INSERT INTO bassin (nom, description,refCapteur) VALUES (:nom,:descript,:refCapteur)");
    $addBassin->bindParam(':nom', $nom, PDO::PARAM_STR);
    $addBassin->bindParam(':descript', $descript, PDO::PARAM_STR);
    $addBassin->bindParam(':refCapteur', $refCapteur, PDO::PARAM_STR);
    $addBassin->execute();

    $lastId = $objBdd -> lastInsertId();
    echo $lastId;

    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'bassins.php';

    header("Location: http://$serveur$chemin/$page");
}














?>