<?php $titre = "les temperatures"; ?>
<?php ob_start(); ?>

<?php
$idBassin = 0;
$nomBassin = "Bassin inconnu";
if ((isset($_GET['nomBassin'])) && (isset($_GET['idBassin']))) {
    $idBassin = intval(htmlspecialchars($_GET['idBassin']));
    $nomBassin = htmlspecialchars($_GET['nomBassin']);
}

//Requete SQL
require "bdd/bddconfig.php";
$objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);

$listeTemperatures = $objBdd->prepare("SELECT * FROM temperature WHERE idBassin = :id ORDER BY date DESC");
$listeTemperatures->bindParam(':id', $idBassin, PDO::PARAM_INT);
$listeTemperatures->execute(); ?>


<article>
    <h1>Les temperatures : <?= $nomBassin; ?></h1>
    <table>
        <thead>
            <tr>
                <th>Date</th>
                <th>Température (°C)</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listeTemperatures as $temp) { ?>
            <tr>
                <td><?php echo $temp['date']; ?></td>
                <td><?php echo $temp['temp']; ?></td>
            </tr>
            <?php }
            $listeTemperatures->closeCursor();
            ?>
        </tbody>
    </table>
</article>

<?php $contenu = ob_get_clean(); ?>
<?php require 'template.php'; ?>


<?php //$listeTemperatures = $objBdd->query("SELECT * FROM temperature WHERE idBassin = $idBassin ORDER BY date DESC"); ?>