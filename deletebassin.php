<?php
require "bdd/bddconfig.php";

$paramOK = false;
// Recup la variables POST et les sécurise
if ((isset($_POST['idBassin']))) {
    $idBassin = intval(htmlspecialchars($_POST['idBassin']));
    $paramOK = true;
}

// INSERT dans la base
if ($paramOK == true) {
    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
        $objBdd -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Supprimer les températures de ce bassin
        $delBassin = $objBdd->prepare("DELETE FROM temperature WHERE idBassin =:id");
        $delBassin->bindParam(':id', $idBassin, PDO::PARAM_INT);
        $delBassin->execute();

        //Supprimer le bassin de la table bassin
        $delBassin = $objBdd->prepare("DELETE FROM bassin WHERE idBassin =:id");
        $delBassin->bindParam(':id', $idBassin, PDO::PARAM_INT);
        $delBassin->execute();
    
    
        // Redirige vers une page différente du dossier courant
        $serveur = $_SERVER['HTTP_HOST'];
        $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        $page = 'bassins.php';
    
        header("Location: http://$serveur$chemin/$page");
        //header("Location: http://localhost:82/formulaires/index.php");
    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE -> getMessage());
    }
} else {
    die('Les paramètres reçus ne sont pas valides');
}














?>