<?php $titre = "Ajout bassin"; ?>
<?php ob_start(); ?>

<?php
session_start();
//Accès seulement si authentifié 
if (isset($_SESSION['logged_in']['login']) !== TRUE) {
    // Redirige vers la page d'accueil (ou login.php) si pas authentifié
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
    $page = 'index.php';
    header("Location: http://$serveur$chemin/$page");
}

?>


<article>
    <h1>Ajouter un bassin</h1>
    <form method="POST" action="insertbassin.php">
        <label for="nom">Nom du bassin :</label><br>
        <input type="text" name="nom" id="nom" placeholder="Nom du bassin"><br>
        <label for="descript">Description :</label><br>
        <textarea name="descript" id="descript" cols="30" rows="10" placeholder="Description du bassin"></textarea><br>
        <label for="refCapteur">Ref du Capteur :</label><br>
        <input type="text" name="refCapteur" id="refCapteur" placeholder="Id du capteur"><br>
        <input type="submit" value="Enregistrer">
    </form>
</article>



<?php $contenu = ob_get_clean(); ?>
<?php require 'template.php'; ?>